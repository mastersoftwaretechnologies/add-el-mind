-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 26, 2014 at 09:33 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `addelmind`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `price` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  `Location` varchar(100) NOT NULL,
  `tittle` varchar(100) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `pimage` text NOT NULL,
  `left_position` varchar(1000) NOT NULL,
  `top_position` varchar(1000) NOT NULL,
  `tag_title` varchar(1000) NOT NULL,
  `tag_dec` varchar(1000) NOT NULL,
  `categories` text NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`pid`, `price`, `Description`, `Location`, `tittle`, `uid`, `pimage`, `left_position`, `top_position`, `tag_title`, `tag_dec`, `categories`) VALUES
(47, '132', '1231', '12', '', '100006664832493', '10000666483249320141191392813551230.jpg', '166,982,491,575', '298.6000061035156,150.60000610351562,282.6000061035156,592', 'wrte,werter,Issue#143:Outlook  calendar event deletee,ert', 'er,ert,et,ert', ',,,,,,,,,,,,,,,,,Movies'),
(48, '20', 'rwer', 'patiala', '', '100006664832493', '10000666483249320141201392869951009.jpg', '', '', '', '', ',Antiques,,,,,Hobbies,,,,, Musical Instruments'),
(49, '20', '', 'patiaas', '', '100000482127210', '10000048212721020141241393221231791.jpg', '', '', '', '', ',Antiques,,,,,Hobbies,,,Electronics,, Musical Instruments,,Books,Music'),
(50, '23', '', 'sd sasd', '', '100000482127210', '10000048212721020141241393221487537.jpg', '', '', '', '', '{"1":"Antiques","2":"Cameras and Accessories","3":"Phones and Telephones","4":"Collectibles","14":"Music","16":"Video","17":"Movies","18":"Clothing"}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `image` blob NOT NULL,
  `displayName` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fbid`, `username`, `full_name`, `image`, `displayName`) VALUES
(12, '100000842631410', 'kpatyal1', 'karan patiyal', 0x2f75706c6f6164732f356265636261386132333266343934612e6a7067, ''),
(28, '100000482127210', 'harinder.singh.3975', 'HarinDer SinGh', 0x68747470733a2f2f67726170682e66616365626f6f6b2e636f6d2f3130303030303438323132373231302f706963747572653f747970653d6e6f726d616c, 'HarinDer SinGh'),
(29, '100006664832493', 'solu.solu.165', 'Solu Solu', 0x68747470733a2f2f67726170682e66616365626f6f6b2e636f6d2f3130303030363636343833323439332f706963747572653f747970653d6e6f726d616c, 'Solu Solu');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
