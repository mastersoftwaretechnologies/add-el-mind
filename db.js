// Mysql Connection
var mysql = function localConnect(){
    //Node Mysql dependency npm install mysql@2.0.0-alpha7
    return require('mysql').createConnection({
        hostname: 'localhost:810',
        user: 'root',
        password: '',
        database: 'addelmind'
    });
}
module.exports.localConnect = mysql;